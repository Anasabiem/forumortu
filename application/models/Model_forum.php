<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_forum extends CI_Model{

  public function select($table){
    return $this->db->get($table);
  }

  public function selectwhere($table,$data){
    return $this->db->get_where($table, $data);
  }

  public function delete($where,$table){
    $this->db->where($where);
    $this->db->delete($table);
  }

  public function update($table,$data,$where){
    $this->db->update($table,$data,$where);
  }

  public function insert($table,$data){
    $this->db->insert($table,$data);
  }

  public function list_forum(){
    $this->db->select('thread.*, user.*,level.*');
    $this->db->join('user', 'user.id_user = thread.id_user');
    $this->db->join('level', 'user.id_level = level.id_level');
    $this->db->from('thread');
    $this->db->order_by('tanggal_thread', 'DESC');
    $data = $this->db->get();
    return $data;
  }

  public function selectatt(){
    $this->db->select('thread.*, user.*');
    $this->db->join('user', 'thread.id_user=user.id_user');
    $this->db->from('thread');
    $this->db->where("user.id_level",11);
    $this->db->order_by('tanggal_thread') ;
    $this->db->limit(3);
    $data=$this->db->get();
    return $data;
  }

  public function detail_forum($id){
    $this->db->select('thread.*, user.*, level.*');
    $this->db->join('user', 'thread.id_user=user.id_user');
    $this->db->join('level', 'user.id_level = level.id_level');
    $this->db->from('thread');
    $this->db->where("thread.id_thread",$id);
    $data=$this->db->get();
    return $data;
  }

  public function reply_forum($id){
    $this->db->select('reply.*, thread.*, user.*, level.*');
    $this->db->join('thread', 'thread.id_thread = reply.id_thread');
    $this->db->join('user', 'user.id_user = reply.id_user');
    $this->db->join('level', 'user.id_level = level.id_level');
    $this->db->from('reply');
    $this->db->where('reply.id_thread', $id);
    $this->db->order_by('id_reply', 'ASC');
    $data = $this->db->get();
    return $data;
  }

  public function balas_reply_forum($id){
    $this->db->select('balas_reply.*, thread.*, user.*, reply.*');
    $this->db->join('thread', 'thread.id_thread = balas_reply.id_thread');
    $this->db->join('user', 'user.id_user = balas_reply.id_user');
    $this->db->join('reply', 'reply.id_reply = balas_reply.id_reply');
    $this->db->from('balas_reply');
    $this->db->where('balas_reply.id_thread', $id);
    $this->db->order_by('id_balas_reply', 'ASC');
    $data = $this->db->get();
    return $data;
  }

  public function list_thread($id){
    $this->db->select('thread.*, user.*');
    $this->db->join('user', 'user.id_user = thread.id_user');
    $this->db->from('thread');
    $this->db->where('thread.id_user', $id);
    $this->db->order_by('id_thread', 'DESC');
    $data = $this->db->get();
    return $data;
  }
}
