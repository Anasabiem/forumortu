<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('Modelku');
		$this->load->model('Model_forum');
	}
	public function index(){
		$this->load->view('v_daftar');
		}

	public function buat(){
		if(isset($_POST['btnSimpan'])){
			$config = array('upload_path' => './media/gambar_user/' ,'allowed_types' => 'gif|jpg|png|jpeg' );
			$this-> load -> library('upload', $config);
			if ($this->upload->do_upload('gambar_user')) 
			{
				$upload_data = $this -> upload -> data ();
				$nama = $this -> input -> post ('nama');
		       	$username = $this -> input -> post ('user');
		        $password = $this -> input -> post ('pass');
		        $foto = "media/gambar_user/".$upload_data['file_name'];
		        $data = array(
		        'nama'=>$nama,
		        'username'=>$username,
		        'password'=>md5($password),
		        'gambar'=>$foto,
		        'id_level'=>12
		        );
		        $insert_data = $this->db->insert('user',$data);
			}
      if ($insert_data >= 0) {
        header('location:'.base_url("Login"));
       } else{
       header('location:'.base_url("Daftar"));
       }
    }else{
      echo "gagal";
    }

  }	

}

?>
