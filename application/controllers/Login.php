<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Modelku');
		$this->load->model('Model_forum');
	}
	public function index(){
		if ($this->session->userdata('status') == 'login') {
			redirect('Forum');
		}else{
			$this->load->view('v_login');
		}
	}
	
	function login(){
		$user = $this->input->post('username');
		$psw = md5($this->input->post('password'));
		$data = array('username' => $user ,
			'password'=> $psw);
		$cek = $this->Modelku->cek_login("user",$data)->num_rows();
		$cek1 = $this->Modelku->cek_login("user",$data)->row();

		if ($cek > 0) {
			$data_session = array(
				'id' => $cek1->id_user,
				'level'=>$cek1->id_level,
				'status' => "login"
			);
			$this->session->set_userdata($data_session);
			// if ($cek1 -> id_level == '11') {
					redirect(base_url("Forum?"));
			// 	}else if ($cek1 -> id_level == '12') {
			// 		redirect (base_url("Forum?"));
			// 	}
		}else{
				echo "Gagal";

			redirect(base_url("Login"));
			}
	}
	function Logout(){
		$this->session->sess_destroy();
		redirect(base_url('Login'));
	}
}

?>
