<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller{
function __construct(){
    parent::__construct();
    $this->load->model('Modelku');
    $this->load->model('Model_forum');
  }

  public function index(){
    if(!$this->session->userdata('status') == 'login'){
      redirect('Login');
    }else{
      $body['attention'] = $this->Model_forum->selectatt();
      $body['body'] = 'v_forum';
      $body['thread'] = $this->Model_forum->list_forum();
      $id = $this->session->userdata('id');
      $body['user'] = $this->Model_forum->selectwhere('user', array('id_user'=>$id));
  		$this->load->view('side/pages', $body);
    }
  }

  public function detail(){
    $body['body'] = 'd_forum';
    $id = $this->uri->segment(3);
    $body['detail'] = $this->Model_forum->detail_forum($id);
    $body['reply'] = $this->Model_forum->reply_forum($id);
    $body['bls_reply'] = $this->Model_forum->balas_reply_forum($id);
    $id = $this->session->userdata('id');
    $body['user'] = $this->Model_forum->selectwhere('user', array('id_user'=>$id));
    $this->load->view('side/pages', $body);
  }

  public function balas_forum(){
    $body['body'] = 'balas_forum';
    $id_thread = $this->uri->segment(3);
    $id = $this->session->userdata('id');
    $body['user'] = $this->Model_forum->selectwhere('user', array('id_user'=>$id));
    $body['id_thread'] = $id_thread;
    $this->load->view('side/pages', $body);
  }

  public function buat_forum(){
    $body['body'] = 'buat_forum';
    $id = $this->session->userdata('id');
    $body['user'] = $this->Model_forum->selectwhere('user', array('id_user'=>$id));
    $this->load->view('side/pages', $body);
  }

  public function profile_user(){
    $body['body'] = 'v_user';
    $id = $this->session->userdata('id');
    $count = $this->Model_forum->selectwhere('thread', array('id_user'=>$id));
    $row = $count->num_rows();
    $body['count'] = $row;
    $body['user'] = $this->Model_forum->selectwhere('user', array('id_user'=>$id));
    // $body['list_thread'] = $this->Model_forum->selectwhere('thread', array('id_user'=>$id));
    $body['list_thread'] = $this->Model_forum->list_thread($id);
    $this->load->view('side/pages', $body);
  }

  public function add_threat(){
    $judul = $this->input->post('judul_thread');
    $isi = $this->input->post('isi');
    $user = $this->session->userdata('id');
    $now = date('Y-m-d');
    $data = array('judul ' =>$judul ,
    'isi'=> $isi,
    'id_user'=>$user,
    'tanggal_thread'=> $now
     );
    $berhasil = $this->Modelku->insert('thread',$data);
    if ($berhasil > 0) {
      echo "Berhasil";
      redirect(base_url('Forum'));
    } else {
      echo "Gagal";
      redirect(base_url('Forum'));
    }
  }

  public function add_reply(){
    $isi = $this->input->post('isi_reply');
    $user = $this->session->userdata('id');
    $thread = $this->input->post('id_thread');
    $data = array(
      'isi_reply'=> $isi,
      'id_user'=>$user,
      'id_thread'=>$thread
     );
    $berhasil = $this->Modelku->insert('reply',$data);
    if ($berhasil > 0) {
      echo "Berhasil";
      redirect(base_url('Forum'));
    } else {
      echo "Gagal";
      redirect(base_url('Forum'));
    }
  }

  public function balas_add_reply(){
    $isi = $this->input->post('isi_bls_reply');
    $now = date('Y-m-d h:i:s');
    $user = $this->session->userdata('id');
    $thread = $this->input->post('id_thread');
    $reply = $this->input->post('id_reply');
    $data = array(
      'isi_balas_reply'=> $isi,
      'id_user'=>$user,
      'id_thread'=>$thread,
      'id_reply'=>$reply,
      'tanggal_balas'=> $now
     );
    $berhasil = $this->Modelku->insert('balas_reply',$data);
    if ($berhasil > 0) {
      echo "Berhasil";
      redirect(base_url('Forum/detail'));
    } else {
      echo "Gagal";
      redirect(base_url('Forum'));
    }
  }

  public function delete(){
    $id = $this->uri->segment(3);
    $this->Model_forum->delete(array('id_thread'=>$id), 'thread');
    redirect('Forum/profile_user');
  }

  public function edit(){
    $body['body'] = 'edit_forum';
    $id_thread = $this->uri->segment(3);
    $id = $this->session->userdata('id');
    $body['user'] = $this->Model_forum->selectwhere('user', array('id_user'=>$id));
    $body['id_thread'] = $id_thread;
    $body['isi_thread'] = $this->Model_forum->selectwhere('thread', array('id_thread'=>$id_thread));
    $this->load->view('side/pages', $body);
  }

  public function edit_thread(){
    $data['judul'] = $this->input->post('judul_thread');
    $data['isi'] = $this->input->post('isi');
    $id_thread = $this->input->post('id_thread');
    $id_user = $this->input->post('id_user');
    $update = array('id_thread' => $id_thread, 'id_user' => $id_user);
    $this->Model_forum->update('thread', $data, $update);
    redirect('Forum/profile_user');
  }
  public function e_user(){
    $config = array('upload_path' => './media/gambar_user/' ,'allowed_types' => 'gif|jpg|png|jpeg' );
      $this-> load -> library('upload', $config);
      if ($this->upload->do_upload('gambar_user')) 
      {
                $upload_data = $this -> upload -> data ();
                $id= $this->input->post("id_user");
                $where['id_user'] = $id;
                $nama = $this -> input -> post('nama');
                $username = $this -> input -> post('user');
                $password = $this -> input -> post('pass');
                $foto = "media/gambar_user/".$upload_data['file_name'];
            $data = array(
            'nama' => $nama,
            'username' => $username,
            'password'=>md5($password),
            'gambar'=>$foto

            );
            $update_data = $this->db->update('user',$data, $where);
            }
          if ($update_data >= 0) {
        redirect(base_url().'Forum/profile_user');
           } else{
        redirect(base_url().'Forum/profile_user');
           }
    }

}
