<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<br>
<div class="container">
  <ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url('Forum'); ?>">Forum</a></li>
    <li class="breadcrumb-item active">--DetailForum--</li>
  </ul>
</div>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-dark text-white">
          Edit thread/forum diskusi
        </div>

        <?php foreach ($isi_thread->result() as $i): ?>
          <form class="" action="<?php echo base_url('Forum/edit_thread') ?>" method="post">
            <div class="card-header">
              <input type="text" name="judul_thread" value="<?php echo $i->judul; ?>" placeholder="Judul Thread">
            </div>
            <div class="card-body">
              <script src ="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
              <textarea class="ckeditor" id="ckeditor" name="isi" rows="8" cols="80"><?php echo $i->isi; ?></textarea><br>
              <input type="text" name="id_thread" value="<?php echo $i->id_thread; ?>">
              <input type="text" name="id_user" value="<?php echo $i->id_user; ?>" hidden>
              <button type="submit" name="button" class="btn btn-primary">Post</button>
            </div>
          </form>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>
