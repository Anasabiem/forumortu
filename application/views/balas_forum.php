<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script src ="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<br>
<div class="container">
  <ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url('Forum'); ?>">Forum</a></li>
    <li class="breadcrumb-item active">--Balas Forum--</li>
  </ul>
</div>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-dark text-white">
          Balas Forum
        </div>
        <div class="card-body">
          <form class="" action="<?php echo base_url(); ?>Forum/add_reply" method="post">
            <textarea class="ckeditor" id="ckeditor" name="isi_reply" rows="8" cols="80"></textarea><br>
            <input type="text" name="id_user" value="
              <?php foreach ($user->result() as $u): ?>
                <?php echo $u->id_user; ?>
              <?php endforeach; ?>
            " hidden>
            <input type="text" name="id_thread" value="

                <?php echo $id_thread; ?>

            " hidden>
            <button type="submit" name="button" class="btn btn-primary">Post</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
