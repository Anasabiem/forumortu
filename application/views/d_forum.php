<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<br>
<div class="container">
  <ul class="breadcrumb" style="background-color: #3498db">
    <li class="breadcrumb-item"><a style="color: #2c3e50" href="<?php echo base_url('Forum'); ?>">Forum</a></li>
    <li class="breadcrumb-item active" style="color: #fff">--DetailForum--</li>
  </ul>
</div>

<div class="container">
  <?php foreach ($detail->result() as $d): ?>
    <a class="btn btn-success" role="button" href="<?php echo base_url('Forum/balas_forum/'); ?><?php echo $d->id_thread; ?>">Komentar Thread Ini</a>
  <?php endforeach; ?>
</div>
<br>

<div class="container">
  <div class="row">
    <?php foreach ($detail->result() as $d): ?>
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header bg-dark text-white">
            <?php echo $d->tanggal_thread; ?>
          </div>
          <div class="card-header">
            <div class="row">
              <div class="col-lg-1">
                <div class="media">
                 <img style="height:70px;" src="<?php echo base_url().$d -> gambar ?>" alt="Profile Picture">
               </div>
             </div>
             <div class="col-lg-10">
              <a href="#">
                <?php echo $d->nama; ?><br></a>
                <p><?php echo $d->level; ?></p>
              </div>
            </div>
          </div>
          <div class="card-body">
            <p style="font-weight: bold;"><i class="fa fa-quote-right"></i>
              <?php echo $d->judul; ?>
            </p>
            <p>
              <?php echo $d->isi; ?>

            </p>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <br>
      <hr>
      <p class="text-center"><strong>Kolom diskusi</strong></p>
      <hr>
    </div>
  </div>

  <!-- reply -->
  <?php
  $no = 1;
  ?>
  <?php foreach ($reply->result() as $r): ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header text-white">
            <div class="row">
              <div class="col-lg-1">
                <div class="media">
                  <img style="height:70px;" src="<?php echo base_url().$r -> gambar ?>" alt="Profile Picture">
                </div>
              </div>
              <div class="col-lg-10" style="color: #000;  margin-top: 10px;">
                <a href="#" style="color: #000">
                  <?php echo $r->nama; ?>
                </a>
                <p style="opacity: 90%; font-size: 12px;">
                  <?php echo $r->level; ?>
                </p>
              </div>
              <div class="col-lg-1">
                <a href=""><span class="float-right">#<?php echo $no; ?></span></a>
              </div>
            </div>
          </div>
          <div class="card-body"><?php echo $r->isi_reply; ?><hr>
            <?php foreach ($bls_reply-> result() as $k) { ?>
              <?php if ($r->id_reply == $k->id_reply) { ?>
                <div class="col-md-12">
                  <div class="card">
                    <div class="row">
                      <div class="col-md-2" style="margin-top: 10px"> 
                        <img style="border-radius: 50%;width: 40%; margin-left: 10px;" src="<?php echo base_url().$k -> gambar; ?>" alt="Profile Picture">
                      </div>
                      <div class="col-md-10" style="margin-top: 10px">
                        <div style="margin-left: -10%">
                          <p><strong><?php echo $k -> nama; ?></strong></p>
                          <p style="margin-top: -10px;"><?php echo $k -> isi_balas_reply; ?></p>
                          <p class="float-right mr-2" style="color: #7f8c8d; font-size: 9px;"><?php echo $k -> tanggal_balas; ?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br>
                </div>
              <?php } ?>
            <?php } ?>
            <br>
            <!-- </div> -->
            <div class="card-footer" style="background-color: #fff; border-top: 0px;">
              <li class="nav-item float-right"   style="list-style-type: none; float: right; padding-right: 30px; "><a data-toggle="modal" data-target="#exampleModal<?php echo $r->id_reply?>" href=""><i class="fa fa-reply" aria-hidden="true"> Komentari</i></a>
              </li>
              <br>
              <!-- Modal -->

              <div class="modal fade" id="exampleModal<?php echo $r->id_reply?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header" style="background-color: #27ae60">
                      <h5 class="modal-title" id="exampleModalLabel">(+) Masukan Komentar</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     <form autocomplete="off" method="post" action="<?php echo base_url('Forum/balas_add_reply') ?>" enctype="multipart/form-data">
                       <div class="form-group">
                        <textarea maxlength="5000" data-msg-required="Balas komentar....." placeholder="Balas komentar....." rows="10" class="form-control" name="isi_bls_reply" required></textarea>
                        <input type="text" name="id_reply" value="<?php echo $r->id_reply ?>" hidden>
                        <?php foreach ($detail->result() as $d): ?>
                          <input type="text" name="id_thread" value="<?php echo $d->id_thread; ?>" hidden>
                        <?php endforeach; ?>

                      </div>
                      <button type="submit" class="btn btn-primary float-right mr-2" >Kirim</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>  
          </div>

          <br>
        </div>
      </div>
    </div>
  </div>
  <br>
  <?php $no++; ?>
<?php endforeach; ?>
<!-- reply -->

<br>

<div class="row">
  <div class="col-lg-12">
    <ul class="pagination justify-content-center">
      <li class="page-item"><a class="page-link" href="#">Previous</a></li>
      <li class="page-item"><a class="page-link" href="#">1</a></li>
      <li class="page-item"><a class="page-link" href="#">2</a></li>
      <li class="page-item"><a class="page-link" href="#">3</a></li>
      <li class="page-item"><a class="page-link" href="#">Next</a></li>
    </ul>
  </div>
</div>
</div>
