<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->model('Modelku');
?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Forum Diskusi Orang Tua</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="shortcut icon" href="<?php echo base_url() ?>favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <!-- <body oncontextmenu='return false;' onkeydown='return false;' onmousedown='return false;'> -->
  <body style="background-color: #e9ebee;">
    <!-- nav -->

  <nav class="navbar bg-dark navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <a href="<?php echo base_url(); ?>" class="navbar-brand">
          <img src="<?php echo base_url(); ?>media/picture/favicon.png" alt="Logo" style="width:40px;">
          <span class="text-white">Forum Diskusi</span>
        </a>
      </div>
      <ul class="navbar nav navbar-right">
        <div class="dropdown m-2">
          <button class="dropdown-toggle-ajax btn btn-success" type="button" data-toggle="dropdown" name="button"><span class="count bg-danger"></span> Notif</button>
          <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
            <!-- <a><span class="count" style="border-radius:10px;"></span> <span class="glyphicon glyphicon-envelope" style="font-size:18px;"></span></a> -->
            <ul class="asd list-group"></ul>
          </div>
        </div>
        <div class="dropdown m-2">
          <?php
            $dat = $this->Modelku->selectwhere('user',$this->session->userdata('id'))->row();
          ?>
          <button class="profile-dropdown-toggle btn btn-success" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="thumbnail-wrapper d32 circular inline">
            <!-- <img src="<?php echo base_url() ?>" alt="" data-src="<?php echo base_url() ?>master/adm/assets/img/profiles/avatar.jpg" data-src-retina="<?php echo base_url() ?>master/adm/<?php echo base_url() ?>master/adm/assets/img/profiles/avatar_small2x.jpg" width="32" height="32"> -->
            <i class="fa fa-user"></i>
            </span>
          </button>
          <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
            <a href="<?php echo base_url('Forum/profile_user'); ?>" class="dropdown-item"><i class="fa fa-cog"></i> Settings</a>
            <a href="<?php echo base_url('Login/Logout') ?>" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
          </div>
        </div>

      </ul>
    </div>
  </nav>

<!-- end nav -->

<script>
$(document).ready(function(){

 function load_unseen_notification(view = '')
 {
  $.ajax({
   url:"fetch.php",
   method:"POST",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    $('.asd').html(data.notification);
    if(data.unseen_notification > 0)
    {
     $('.count').html(data.unseen_notification);
    }
   }
  });
 }

 load_unseen_notification();


 $(document).on('click', '.dropdown-toggle-ajax', function(){
  $('.count').html('');
  load_unseen_notification('yes');
 });

 setInterval(function(){
  load_unseen_notification();;
 }, 5000);

});
</script>
