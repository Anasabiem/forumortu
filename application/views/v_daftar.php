<?php $this->load->view("side/head2"); ?>
<br>
<div class="container">
  <ul class="breadcrumb">
    <li class="breadcrumb-item active">Daftar Akun</li>
</ul>
</div>
<div class="container">
    <div class="col-md-12">
        <div class="box">
            <h2><i class="fa fa-user-plus"></i> Daftar Akun</h2>
            <form autocomplete="off" method="post" action="<?php echo base_url(). 'Daftar/buat'; ?>" enctype="multipart/form-data">
            	<hr>
                 <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" name="nama" class="form-control" required >
                </div>
                <div class="form-group">
                    <label for="name">Username</label>
                    <input type="text" name="user" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="number">Password</label>
                    <input type="Password" name="pass" class="form-control" required>
                </div>
                <div class="form-group">
				    <label for="exampleFormControlFile1">Pilih Gambar Profile</label>
				    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="gambar_user" required="">
				</div>
				<div>
					<br>
                	<button style="float: right;" class="btn btn-success" name="btnSimpan">Simpan</button>
                </div>
            </form>
            </div>
        </div>
    </div>
    <br>
    <!-- /.container -->

<?php $this->load->view("side/footer"); ?>