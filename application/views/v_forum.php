<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<br>
<div class="container" >
  <ul class="breadcrumb"style="background-color: #3498db">
    <li class="breadcrumb-item active" style="color: #fff; font-weight: bold;"><i class="fa fa-users"></i> Forum</li>
  </ul>
</div>

<div class="container">
  <div class="row">
    <div class="col-lg-9">
      <?php foreach ($thread->result() as $t) { ?>
        <?php if (!$t==null){ ?>
          <div class="card">
          <div class="card-header" style="background-color: #fff; "> 
            <!-- <i class="fa fa-users"> -->
              <img style="border-radius: 50%; width: 5%;" src="<?php echo base_url().$t -> gambar ?>">
            </i> <?php echo $t->nama; ?><span style="font-size:12px; opacity: 0.7; float: right;" > #<?php echo $t->level; ?></span></div>
            <div class="card-body">
              <a style="color: #000000; font-weight: bold;"  href="<?php echo base_url('Forum/detail/'); ?><?php echo $t->id_thread; ?>"><i class="fa fa-quote-right"></i>  <?php echo $t->judul?></a>
              <p><?php echo  substr($t->isi,0,100); ?></p>
            </div>
            <div class="card-footer navbar-expand-sm" style="background-color: #fff">
              <ul class='navbar-nav mr-auto' style=" opacity: 0.5; font-size: 12px;">
                <li class="nav-item" style="padding-right: 30px"><i class="far fa-calendar-alt"></i> <?php echo $t->tanggal_thread; ?> </li>
                <li class="nav-item" style="padding-right: 30px"><i class="fas fa-comments"> 4</i> </li>
              </ul>
              <ul class='navbar-nav mr-auto' style=" float: right; opacity: 0.5; font-size: 12px;">
                <li class="nav-item" style="float: right; padding-right: 30px"><a href="<?php echo base_url('Forum/detail/'); ?><?php echo $t->id_thread; ?>"><i class="fa fa-reply" aria-hidden="true"> Balas</i></a>
                </li>
              </ul>
            </div>
          </div>
          <br>
        <?php } else{ ?><div class="card">
          <div class="card-header" style="background-color: #fff; "><p>Tidak Ada Thread Terbaru</p> </div></div><?php }?>
        <?php } ?>
      </div>
      <div class="col-lg-3">
        <div class="card">
          <div class="card-body">
            <p>Jika ada yang ingin di diskusikan atau ada yang dipertanyakan silahkan membuat Thread di "New Post Thread" di bawah ini.</p>
            <a class="btn btn-primary" style="float: right; width: 100%" role="button" href="<?php echo base_url('Forum/buat_forum'); ?>">Post New Thread</a>
          </div>
        </div>
        <br>
        <div class="card">
          <ul class="breadcrumb"style="background-color: #3498db">
            <li class="breadcrumb-item active" style="color: #fff; "><i class="fa fa-users"> Forum Penting</i></li>
          </ul>
          <div class="card-body">
            <?php foreach ($attention->result() as $t) { ?>
              <p><a  style="color: #000;" href="<?php echo base_url('Forum/detail/'); ?><?php echo $t->id_thread; ?>"><?php echo $t->judul?></a>
                <span style="float: right; opacity: 0.4; font-size: 10px; color: #000"><?php echo $t->tanggal_thread; ?></span></p><hr>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br><br><br><br><br><br>
