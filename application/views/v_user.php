<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
      <div class="row">
        <div class="col">
          <ul class="breadcrumb">
            <li><a style="color: #10ac84" href="<?php echo base_url() ?>">Home</a></li>
            <li class="active">/Dasboard User</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h1>Profile Dashboard</h1>
        </div>
      </div>
    </div>
<!-- <div class="card-header bg-dark text-white">
          Dashboard Profile
        </div> -->
<!-- <div class="jumbotron" style="">
=======
<div class="jumbotron" style="background-color: #7f8c8d">
>>>>>>> 5bfb02e9bb09893188ad3def3b0b59fb43ffabca
  <h1 class="">My Dashboard Profile</h1>
</div> -->
<br>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-dark text-white">
          My Profil
        </div>
        <br>
        <div class="row">
          <div class="col-lg-2">
            <div class="media" style="margin-top: 10px;">
              <?php foreach ($user->result() as $img) { ?>
              <img style="height:120px; margin-left: 10px;" src="<?php echo base_url().$img -> gambar ?>" alt="">
            <?php } ?>
            </div>
          </div>
          <div class="col-lg-10" style="margin-top: 10px">
            <?php foreach ($user->result() as $u): ?>
              <h3><?php echo $u->nama; ?></h3>
              <!-- <p>User ID: <?php echo $u->id_user; ?></p> -->
            <?php endforeach; ?>
              <p>Jumlah thread: <?php echo $count; ?></p>
              <a title="Edit" style="padding: 10px;" class=" float-right mr-2" role="button" href="" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-magic"></i></a>
          </div>

          <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" style="background-color: #27ae60">
                  <h5 class="modal-title" id="exampleModalLabel">(+) Edit Profile</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                   <form autocomplete="off" method="post" action="<?php echo base_url(). 'Forum/e_user'; ?>" enctype="multipart/form-data">
                           <?php foreach ($user->result() as $u){ ?>
                       <div class="form-group">
                          <label for="name">Nama</label>
                          <input type="text" name="nama" class="form-control" value="<?php echo $u-> nama; ?>" required >
                      </div>
                      <div class="form-group">
                          <label for="name">Username</label>
                          <input type="text" name="user" class="form-control" value="<?php echo $u-> username; ?>" required>
                      </div>
                      <div class="form-group">
                          <label for="number">Password</label>
                          <input type="Password" name="pass" class="form-control" value="<?php echo $u-> password; ?>" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlFile1">Pilih Gambar Profile</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="gambar_user" value="<?php echo base_url(). $u-> gambar ?>">
                    </div>
                      <input type="text" name="id_user" value="<?php echo $u->id_user; ?>" hidden>
                      <hr>
                      <button type="submit" class="btn btn-primary float-right mr-2" >Simpan Perubahan</button>
                          <?php } ?>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <br>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h3>Daftar Thread</h3>
        </div>
        <?php foreach ($list_thread->result() as $l): ?>
          <div class="card-body">
            <a style="font-weight: bold;" href="<?php echo base_url('Forum/detail/'); ?><?php echo $l->id_thread; ?>">#<?php echo $l->judul; ?></a><br>
            <span><?php echo substr($l->isi,0,100); ?></span>
            <a style="padding: 10px" title="hapus" class=" float-right mr-2" role="button" href="<?php echo base_url('Forum/delete/'); ?><?php echo $l->id_thread; ?>"><i class="fa fa-trash"></i></a>
            <a style="padding: 10px" title="Edit" class=" float-right mr-2" role="button" href="<?php echo base_url('Forum/edit/'); ?><?php echo $l->id_thread; ?>"><i class="fa fa-magic"></i></a>
            <hr>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>
<br><br><br>
