<?php
//fetch.php;
if(isset($_POST["view"]))
{
 include("connect.php");
 if($_POST["view"] != '')
 {
  $update_query = "UPDATE reply SET unseen=1 WHERE unseen=0";
  mysqli_query($connect, $update_query);
 }
 $query = "SELECT * FROM reply ORDER BY id_reply DESC LIMIT 5";
 $result = mysqli_query($connect, $query);
 $output = '';

 if(mysqli_num_rows($result) > 0)
 {
  while($row = mysqli_fetch_array($result))
  {
   $output .= '
   <li class="list-group-item">
    <a href="#">
     '.$row["isi_reply"].'
    </a>
   </li>

   ';
  }
 }
 else
 {
  $output .= '<li><a href="#" class="text-bold text-italic">No Notification Found</a></li>';
 }

 $query_1 = "SELECT * FROM reply WHERE unseen=0";
 $result_1 = mysqli_query($connect, $query_1);
 $count = mysqli_num_rows($result_1);
 $data = array(
  'notification'   => $output,
  'unseen_notification' => $count
 );
 echo json_encode($data);
}
?>
