-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2018 at 08:41 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forum`
--

-- --------------------------------------------------------

--
-- Table structure for table `balas_reply`
--

CREATE TABLE `balas_reply` (
  `id_balas_reply` int(9) NOT NULL,
  `isi_balas_reply` text NOT NULL,
  `id_thread` int(9) NOT NULL,
  `id_user` int(9) NOT NULL,
  `id_reply` int(9) NOT NULL,
  `tanggal_balas` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balas_reply`
--

INSERT INTO `balas_reply` (`id_balas_reply`, `isi_balas_reply`, `id_thread`, `id_user`, `id_reply`, `tanggal_balas`) VALUES
(1, 'yi', 4, 1221, 6, '2018-12-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(9) NOT NULL,
  `level` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `level`) VALUES
(11, 'Admin'),
(12, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `id_reply` int(9) NOT NULL,
  `isi_reply` text NOT NULL,
  `id_thread` int(9) NOT NULL,
  `id_user` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reply`
--

INSERT INTO `reply` (`id_reply`, `isi_reply`, `id_thread`, `id_user`) VALUES
(1, '<p>dgdgd</p>\r\n', 4, 1221),
(2, '<p>asasasa</p>\r\n', 4, 1221),
(3, '<p>dds</p>\r\n', 4, 1221),
(4, '<p>opo</p>\r\n', 4, 1221),
(5, '<ol>\r\n	<li>ds</li>\r\n</ol>\r\n', 4, 1221),
(6, '<p>asa</p>\r\n', 4, 1221);

-- --------------------------------------------------------

--
-- Table structure for table `thread`
--

CREATE TABLE `thread` (
  `id_thread` int(9) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `tanggal_thread` date NOT NULL,
  `id_user` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `thread`
--

INSERT INTO `thread` (`id_thread`, `judul`, `isi`, `tanggal_thread`, `id_user`) VALUES
(1, 'df', '<p>sxusyxjo;</p>\r\n', '2018-11-26', 1221),
(2, 'hjuki', '<p>cghvbhknjk;l</p>\r\n', '2018-11-26', 1222),
(3, 'vgbh', '<p>1212</p>\r\n', '2018-11-26', 1222),
(4, 'Rusak', '<p>Penakmu</p>\r\n', '2018-11-29', 1221);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(9) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `id_level` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama`, `gambar`, `id_level`) VALUES
(1221, 'pri', 'e060bb629c10e1b143614cc1e9ccdc67', 'Aprian Rangga', 'media/gambar_user/logo_hari_santri_nasional.png', 12),
(1222, 'jon', '1f323e62200bcb501b449bdeff1ec931', 'Jonathan', 'media/gambar_user/Pizza_Hut_logo_svg_.png', 11);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `balas_reply`
--
ALTER TABLE `balas_reply`
  ADD PRIMARY KEY (`id_balas_reply`),
  ADD KEY `id_reply` (`id_reply`),
  ADD KEY `id_thread` (`id_thread`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `reply`
--
ALTER TABLE `reply`
  ADD PRIMARY KEY (`id_reply`),
  ADD KEY `id_thread_reply` (`id_thread`),
  ADD KEY `id_user_reply` (`id_user`);

--
-- Indexes for table `thread`
--
ALTER TABLE `thread`
  ADD PRIMARY KEY (`id_thread`),
  ADD KEY `id_user_thread` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_level_user` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `balas_reply`
--
ALTER TABLE `balas_reply`
  MODIFY `id_balas_reply` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `reply`
--
ALTER TABLE `reply`
  MODIFY `id_reply` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `thread`
--
ALTER TABLE `thread`
  MODIFY `id_thread` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1223;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `balas_reply`
--
ALTER TABLE `balas_reply`
  ADD CONSTRAINT `balas_reply_ibfk_1` FOREIGN KEY (`id_reply`) REFERENCES `reply` (`id_reply`),
  ADD CONSTRAINT `balas_reply_ibfk_2` FOREIGN KEY (`id_thread`) REFERENCES `thread` (`id_thread`),
  ADD CONSTRAINT `balas_reply_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `reply`
--
ALTER TABLE `reply`
  ADD CONSTRAINT `reply_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reply_ibfk_2` FOREIGN KEY (`id_thread`) REFERENCES `thread` (`id_thread`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `thread`
--
ALTER TABLE `thread`
  ADD CONSTRAINT `thread_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
